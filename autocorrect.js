const AUTOCORRECT_ATTR = '_autocorrect';
const AUTOCORRECT_DEFAULT = false;
// TODO: Add more dictionaries
const DICTIONARIES = {
    'en_US': 'dicts/dict.txt',
    'en_GB': 'dicts/dict.txt',
    'ro_RO': 'dicts/dict_ro_RO.txt'
}
const DEFAULT_LOCALE = 'en_US';

const MAX_EDIT_DISTANCE = 2;

/** @type {string} */
var locale;
/** @type {KeyboardLayout} */
var keyboardLayout;
/** @type {boolean} */
var _init = false;
/** @type {{[x: string]: number}} A dictionary with lowercase keys which map to number values. */
var dict = {};

var $lastClickedElement = null;

(async function () {
    await Init();
})();

async function Init() {
    await browser.storage.sync.get({ 'locale': DEFAULT_LOCALE }).then(function (item) {
        locale = item.locale;
        if (!locale) {
            locale = DEFAULT_LOCALE;
        }
    });
    keyboardLayout = await KeyboardLayout.loadFrom(`layouts/${locale}.json`); /* eslint-disable-line no-undef */
    dict = await LoadDictionary();

    browser.runtime.onMessage.addListener(function (request) {
        if (request === 'disable_autocorrect_last_element') {
            $lastClickedElement.attr(AUTOCORRECT_ATTR, false);
            let id;
            if (id = $lastClickedElement.attr('id')) {
                browser.storage.local.get(['disabled_inputs', 'enabled_inputs']).then(data => {
                    /** @type {{id: string, domain: string}[]} */
                    let disabled = data.disabled_inputs;
                    /** @type {{id: string, domain: string}[]} */
                    let enabled = data.enabled_inputs;
                    let domain = window.location.hostname;
                    let obj = {
                        id: id,
                        domain: domain
                    };
                    if (disabled && typeof (disabled.forEach) === 'function') {
                        if (!(obj in disabled)) {
                            disabled.push(obj);
                            browser.storage.local.set({ 'disabled_inputs': disabled });
                        }
                    } else {
                        browser.storage.local.set({ 'disabled_inputs': [obj] });
                    }
                    if (enabled && typeof (enabled.forEach) === 'function') {
                        enabled = enabled.filter(e => !(e.id === obj.id && e.domain === obj.domain));
                        browser.storage.local.set({ 'enabled_inputs': enabled });
                    }
                })
            }
        }
        if (request === 'enable_autocorrect_last_element') {
            $lastClickedElement.attr(AUTOCORRECT_ATTR, true);
            let id;
            if (id = $lastClickedElement.attr('id')) {
                browser.storage.local.get(['enabled_inputs', 'disabled_inputs']).then(data => {
                    /** @type {{id: string, domain: string}[]} */
                    let enabled = data.enabled_inputs;
                    /** @type {{id: string, domain: string}[]} */
                    let disabled = data.disabled_inputs;
                    let domain = window.location.hostname;
                    let obj = {
                        id: id,
                        domain: domain
                    };
                    if (enabled && typeof (enabled.forEach) === 'function') {
                        if (!(obj in enabled)) {
                            enabled.push(obj);
                            browser.storage.local.set({ 'enabled_inputs': enabled });
                        }
                    } else {
                        browser.storage.local.set({ 'enabled_inputs': [obj] });
                    }
                    if (disabled && typeof (disabled.forEach) === 'function') {
                        disabled = disabled.filter(d => !(d.id === obj.id && d.domain === obj.domain));
                        browser.storage.local.set({ 'disabled_inputs': disabled });
                    }
                });
            }
        }
    });

    await browser.storage.local.get(['enabled_inputs', 'disabled_inputs']).then(data => {
        /** @type {{id: string, domain: string}[]} */
        let enabled = data.enabled_inputs;
        if (enabled && typeof (enabled.forEach) === 'function') {
            enabled.forEach(input => {
                if (input.domain === window.location.hostname) {
                    $(`#${input.id}`).attr(AUTOCORRECT_ATTR, true);
                }
            });
        }
        /** @type {{id: string, domain: string}[]} */
        let disabled = data.disabled_inputs;
        if (disabled && typeof (disabled.forEach) === 'function') {
            disabled.forEach(input => {
                if (input.domain === window.location.hostname) {
                    $(`#${input.id}`).attr(AUTOCORRECT_ATTR, false);
                }
            });
        }
    });

    let inputs = $('input:not([type]), input[type="text"], textarea, *[contenteditable]');
    $.each(inputs, function () {
        let $input = $(this);
        let autocorrect = $input.attr(AUTOCORRECT_ATTR);
        if (undefined === autocorrect) {
            $input.attr(AUTOCORRECT_ATTR, AUTOCORRECT_DEFAULT);
        }
        $input.on('input', AutoCorrect);
        $input.on('contextmenu', OnContextMenu);
    });

    _init = true;
}

function OnContextMenu() {
    let $this = $(this);
    $lastClickedElement = $this;
}

function AutoCorrect() {
    let $this = $(this);
    if ('true' != $this.attr(AUTOCORRECT_ATTR))
        return;

    let text = $this.val();
    if (text === '')
        return;

    let startPos = this.selectionStart - 1;
    if ($this.attr('contenteditable')) {
        text = $this.text();
        startPos = $this.caret('pos') - 1;
    }
    let line = text.replace('\n', ' ');
    let { start: current, word: currentWord } = getWordAround(line, startPos);
    if (current !== -1 || currentWord !== '')
        return;
    // If we've just pressed a separator key
    // Get the word to its left
    /** @type {string} word */
    let { word, start } = getWordAround(line, startPos - 1);
    // If the word is valid, don't correct it
    if (dict[word.toLowerCase()])
        return;

    let matches = Array.from(getClosestMatchesForWord2(word));
    // If there are no matches for the word, don't do anything
    if (matches.length <= 0)
        return;

    let newtext = text.substr(0, start) + matches[0] + text.substr(start + word.length)
    if ($this.attr('contenteditable')) {
        $this.text(newtext);
    } else {
        $this.val(newtext);
    }
    let sel = start + matches[0].length + 1;
    // Go to the end of the inserted word
    if ($this.attr('contenteditable')) {
        $this.caret('pos', sel);
    } else {
        this.selectionStart = sel;
        this.selectionEnd = sel;
    }
}

/**
 * @returns {object} A dictionary with lowercase keys
 */
async function LoadDictionary() {
    if (true === _init) {
        // this returns the global one
        return dict;
    }

    let url = browser.runtime.getURL(DICTIONARIES[locale]);
    let dict = {};
    await $.get(url).done(function (data) {
        /** @type {string} str */
        let str = data;
        let words = str.split(/[ \n]/);
        let regexp = new RegExp(keyboardLayout.invalidWordPattern, 'g');
        words.forEach(word => {
            let w = word.trim().toLowerCase().replace(regexp, '');
            if (w === '')
                return;
            if (dict[w]) {
                ++dict[w]
            } else {
                dict[w] = 1;
            }
        });
    });
    return dict;
}

/**
 * @returns {string}
 * @param {string} text 
 * @param {number} position 
 * @param {bool} goToRight
 */
function getWordAround(text, position, goToRight = false) {
    // TODO: Should we only go to the left?
    let word = text.charAt(position);
    let i = 1;
    let start = 0;
    // If we're on a separator char we don't do anything
    if (isSeparatorChar(word)) {
        return { word: '', start: -1 }
    } else {
        let stopLeft = false;
        let stopRight = !goToRight;
        while ((position - i >= 0 || position + i < text.length) && (stopLeft === false || stopRight === false)) {
            if (false === stopLeft) {
                if (position - i >= 0) {
                    let char = text.charAt(position - i);
                    // Append to left
                    if (false === isSeparatorChar(char)) {
                        word = char + word;
                    } else {
                        if (false === stopLeft) {
                            stopLeft = true;
                            start = position - i + 1;
                        }
                    }
                }
            }
            if (false === stopRight) {
                if (position + i < text.length) {
                    let char = text.charAt(position + i);
                    // Append to right
                    if (false === isSeparatorChar(char)) {
                        word = word + char;
                    } else {
                        stopRight = true;
                    }
                }
            }
            ++i;
        }
    }

    return { word: word, start: start };
}

/**
 * 
 * @param {string} a 
 * @param {string} b 
 */
function DamerauLevenshtein(a, b) {
    let d = [];
    for (let i = 0; i <= a.length; ++i) {
        d[i] = [i];
    }
    for (let j = 0; j <= b.length; ++j) {
        d[0][j] = j;
    }

    for (let i = 1; i <= a.length; ++i) {
        for (let j = 1; j <= b.length; ++j) {
            let cost = 0;
            if (a.charAt(i - 1) !== b.charAt(j - 1)) {
                cost = 1;
            }
            d[i][j] = Math.min(
                d[i - 1][j] + 1, // deletion
                d[i][j - 1] + 1, // insertion
                d[i - 1][j - 1] + cost // substitution
            );
            if (i > 1 && j > 1 && a.charAt(i) === b.charAt(j - 1) && a.charAt(i - 1) === b.charAt(j)) {
                d[i][j] = Math.min(
                    d[i][j],
                    d[i - 2][j - 2] + cost // transposition
                );
            }
        }
    }

    return d[a.length][b.length];
}

/**
 * 
 * @param {string} word 
 * @param {number} matches 
 */
function* getClosestMatchesForWord2(word, matches = 3) {
    let copy = word;
    word = word.toLowerCase();
    let words = Object.keys(dict);
    /**
     * @type {{[x: string]: number}}
     */
    let distances = {};
    for (let dictword of words) {
        // TODO: Improve the efficency of the loop
        let dist = DamerauLevenshtein(word, dictword);
        if (dist > MAX_EDIT_DISTANCE)
            continue;
        distances[dictword] = dist;
    }

    let sorted = Object.keys(distances).sort((a, b) => doubleCompare(distances, a, dict, b));

    for (let i = 0, j = 0; i < sorted.length && j < matches; ++i) {
        if (sorted[i]) {
            ++j;
            let w = matchCase(copy, sorted[i]);
            yield w;
        }
    }
}

function doubleCompare(array1, a, array2, b) {
    if (array1[a] > array1[b])
        return 1;
    if (array1[a] < array1[b])
        return -1;
    
    if (array2[a] > array2[b])
        return -1;
    if (array2[a] < array2[b])
        return 1;
    
    return 0;
}

/**
 * @returns {string[]}
 * @param {string} word
 */
function* getClosestMatchesForWord(word, num = 3) {
    let copy = word;
    word = word.toLowerCase();
    let candidates = {};

    for (let i = 0; i < word.length; ++i) {
        // Similar characters
        let similars = Array.from(keyboardLayout.getSimilarKeys(word.charAt(i)));
        for (let j = 0; j < similars.length; ++j) {
            let similar = word.substr(0, i) + similars[j] + word.substr(i + 1);
            if (dict[similar]) {
                candidates[similar] = Infinity;
            }
        }

        // Level 1 inserts
        let keys = Array.from(keyboardLayout.getAllKeys());
        for (let j = 0; j < keys.length; ++j) {
            let inserted = word.substr(0, i) + keys[j] + word.substr(i);
            if (dict[inserted]) {
                if (candidates[inserted]) {
                    candidates[inserted] += dict[inserted];
                } else {
                    candidates[inserted] = dict[inserted];
                }
            }
        }

        // Level 1 delete
        let deleted = word.substr(0, i) + word.substr(i + 1);
        if (dict[deleted]) {
            if (candidates[deleted]) {
                candidates[deleted] += dict[deleted];
            } else {
                candidates[deleted] = dict[deleted];
            }
        }

        // Level 1 displacements
        let displacements = Array.from(keyboardLayout.getSurroundingKeys(word.charAt(i)));
        for (let j = 0; j < displacements.length; ++j) {
            let disp = word.substr(0, i) + displacements[j] + word.substr(i + 1);
            let similars = Array.from(keyboardLayout.getSimilarKeys(displacements[j]));
            // Level 1 displacements + similar characters
            for (let k = 0; k < similars.length; ++k) {
                let similar = disp.substr(0, j) + similars[k] + disp.substr(j + 1);
                if (dict[similar]) {
                    candidates[similar] = Infinity;
                }
            }

            // Level 1 displacements + inserts
            for (let k = 0; k < keys.length; ++k) {
                let inserted = disp.substr(0, i) + keys[k] + disp.substr(i);
                if (dict[inserted]) {
                    if (candidates[inserted]) {
                        candidates[inserted] += dict[inserted];
                    } else {
                        candidates[inserted] = dict[inserted];
                    }
                }
            }

            // Level 1 displacements
            if (dict[disp]) {
                // If it's already a candidate, make it weigh one more time
                if (candidates[disp]) {
                    candidates[disp] += dict[disp];
                } else {
                    candidates[disp] = dict[disp];
                }
            }
        }
    }

    let caseCandidates = {};
    for (let candidate of Object.keys(candidates)) {
        let caseCandidate = matchCase(copy, candidate);
        caseCandidates[caseCandidate] = candidates[candidate];
    }

    let sorted = sortByValues(caseCandidates);
    for (let i = 0, j = 0; i < sorted.length && j < num; ++i) {
        if (sorted[i]) {
            ++j;
            yield sorted[i];
        }
    }
}

/**
 * 
 * @param {string} caseWord 
 * @param {string} word 
 */
function matchCase(caseWord, word) {
    let totalUpper = false;
    let firstUpper = caseWord.charAt(0).toUpperCase() === caseWord.charAt(0);
    if (firstUpper) {
        totalUpper = true;
        for (let i = 1; i < caseWord.length && totalUpper; ++i) {
            if (caseWord.charAt(i).toUpperCase() !== caseWord.charAt(i)) {
                totalUpper = false;
            }
        }
    }

    if (totalUpper) {
        return word.toUpperCase();
    }
    if (firstUpper) {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    }

    return word;
}

/**
 * @return {string[]} The object sorted descending
 * @param {object} obj 
 */
function sortByValues(obj) {
    return Object.keys(obj).sort((a, b) => obj[b] - obj[a]);
}

/**
 * @returns {boolean}
 * @param {string} char
 */
function isSeparatorChar(char) {
    if (char.length > 1) {
        char = char.charAt(0);
    }

    // There are 2 spaces here, one is a regular one with code point 32, 
    // and one is the nonbreaking space i.e. &nbsp; has code point 160
    return ',.!?;  '.indexOf(char) > -1;
}