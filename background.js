browser.contextMenus.create({
    id: "disable-for-input",
    title: "Disable autocorrect",
    contexts: ["editable"],
    onclick: function (info, tab) {
        browser.tabs.sendMessage(tab.id, 'disable_autocorrect_last_element');
    }
});

browser.contextMenus.create({
    id: "enable-for-input",
    title: "Enable autocorrect",
    contexts: ["editable"],
    onclick: function (info, tab) {
        browser.tabs.sendMessage(tab.id, 'enable_autocorrect_last_element');
    }
});