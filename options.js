$(document).ready(function () {
    $('#locale').on('change', function () {
        let locale = $(this).val();
        browser.storage.sync.set({
            'locale': locale
        })
    });

    browser.storage.sync.get('locale').then(item => {
        let locale = item.locale;
        $('#locale').val(locale).change();
    });
});