class KeyboardLayout { /* eslint-disable-line no-unused-vars */
    /**
     * @param {string} locale
     */
    constructor(locale, inst = null) {
        if (inst != null) {
            if (locale != inst.locale) {
                throw new Error(`locale (${locale}) != inst.locale (${inst.locale})`);
            }
            this.matrix = inst.matrix;
            this.similars = inst.similars;
            this.invalidWordPattern = inst.invalidWordPattern;
            this._loaded = true;
            return this;
        }
        /** @type {boolean} */
        this._loaded = false;
        /** @type {string} */
        this.locale = locale;
        /** @type {string[][]} */
        this.matrix = [[]];
        /** @type {object} */
        this.similars = {};
        /** @type {string} */
        this.invalidWordPattern = "";
    }

    /**
     * @param {string} file 
     */
    static loadFrom(file) {
        let url = browser.runtime.getURL(file);
        return $.ajax({
            url: url,
            dataType: 'text'
        }).then(data => {
            return KeyboardLayout.fromJSON(JSON.parse(data));
        });
    }

    /**
    * @returns {string[]}
    * @param {string} char
    */
    *getSurroundingKeys(char) {
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i].length; j++) {
                if (char == this.matrix[i][j]) {
                    if (j > 0) {
                        yield this.matrix[i][j - 1];
                    }

                    if (j + 1 < this.matrix[i].length) {
                        yield this.matrix[i][j + 1];
                    }

                    if (i > 0) {
                        if (j >= 0 && j < this.matrix[i - 1].length) {
                            yield this.matrix[i - 1][j];
                        }
                    }

                    if (i + 1 < this.matrix.length) {
                        if (j >= 0 && j < this.matrix[i + 1].length) {
                            yield this.matrix[i + 1][j];
                        }
                    }

                    if (i > 0) {
                        if (j > 0 && j < this.matrix[i - 1].length) {
                            yield this.matrix[i - 1][j - 1];
                        }

                        if (j >= 0 && j + 1 < this.matrix[i - 1].length) {
                            yield this.matrix[i - 1][j + 1];
                        }
                    }

                    if (i + 1 < this.matrix.length) {
                        if (j > 0 && j < this.matrix[i + 1].length) {
                            yield this.matrix[i + 1][j - 1];
                        }

                        if (j >= 0 && j + 1 < this.matrix[i + 1].length) {
                            yield this.matrix[i + 1][j + 1];
                        }
                    }

                    return;
                }
            }
        }
    }

    /**
     * @param {string} char 
     */
    *getSimilarKeys(char) {
        if (char.length > 1) {
            char = char.charAt(0);
        }

        if (this.similars[char]) {
            yield *this.similars[char];
        }
    }

    *getAllKeys() {
        for (let i = 0; i < this.matrix.length; ++i) {
            yield *this.matrix[i];
        }
    }

    static fromJSON(obj) {
        return new this(obj.locale, obj);
    }
}